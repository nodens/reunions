Réunions Debian France
======================

Réunions publiques Debian France

Le 2nd Mardi de chaque mois à 21h sur le canal IRC **#debian-france** (réseau OFTC)

----------------------------------------

## Réunion IRC 9 Juin 2020

+ [Sujet](reunion-2020-06-09.md)

## Réunion IRC 12 Mai 2020

+ Annulée, seulement deux participants.

## Réunion IRC 14 Avril 2020

+ [Sujet](reunion-2020-04-14.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2020/debian-france.2020-04-14-18.59.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2020/debian-france.2020-04-14-18.59.log.html)

## Réunion IRC 10 Mars 2020

+ [Sujet](reunion-2020-03-10.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2020/debian-france.2020-03-10-20.00.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2020/debian-france.2020-03-10-20.00.log.html)

----------------------------------------

## Réunion IRC 11 Février 2020

+ [Sujet](reunion-2020-02-11.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2020/debian-france.2020-02-11-19.59.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2020/debian-france.2020-02-11-19.59.log.html)

----------------------------------------

## Réunion IRC 14 Janvier 2020

+ [Sujet](reunion-2020-01-14.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2020/debian-france.2020-01-14-19.59.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2020/debian-france.2020-01-14-19.59.log.html)

----------------------------------------

## Réunion IRC 11 Décembre 2019

+ [Sujets](reunion-2019-12-11.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2019/debian-france.2019-12-11-20.11.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2019/debian-france.2019-12-11-20.11.log.html)

----------------------------------------

## Réunion IRC 13 Novembre 2019

+ [Sujets](reunion-2019-11-13.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2019/debian-france.2019-11-13-19.59.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2019/debian-france.2019-11-13-19.59.log.html)

----------------------------------------

## Réunion IRC 09 Octobre 2019

+ [Sujets](reunion-2019-10-09.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2019/debian-france.2019-10-09-18.59.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2019/debian-france.2019-10-09-18.59.log.html)

----------------------------------------

## Réunion IRC 13 Septembre 2019

+ [Framadate](https://framadate.org/debian-fr-reunion-irc)
+ [Sujets](reunion-2019-09.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2019/debian-france.2019-09-13-19.03.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2019/debian-france.2019-09-13-19.03.log.html)
